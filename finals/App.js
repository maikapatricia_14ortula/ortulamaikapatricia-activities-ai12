import * as React from 'react';
import { Button, View, Text, StyleSheet, Image} from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';


function HomeScreen({ navigation }) {
  return (
    <View style={styles.container}>
      <Text style={styles.title}>Home Screen</Text>
      <Text style={{fontWeight:"bold", fontSize: 20, backgroundColor: 'white',borderRadius: 5,marginRight: 15, marginBottom: 15, marginLeft:15, }}>
        A'TIN. Pronounced as 18, it's meant to show that just like how 18 comes before 19, 
        A'TIN comes before SB19</Text>
        <Image style={styles.image1} source={require('./assets/atin.png')} />
        
        

      <Button  
        title="Go to Details"
        onPress={() => navigation.navigate('Details')}
      />
    </View>
  );
}

function DetailsScreen({ navigation }) {
  return (

    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: '#55BCF6' }}>
      <Text style={styles.song}>BACKGROUND INFORMATION</Text>
      <Text style={styles.information}>Origin : Manila, Philippines</Text>
      <Text style={styles.information}>Genres : P-pop , Pop rock, EDM , hip hop, R&B , Ballad</Text>
      <Text style={styles.information}>Years active : 2018-present</Text>
      <Text style={styles.information}>SB19 is a five-member Filipino boy band that debuted in 2018, consisting of Josh, Pablo, Stell, Ken, and Justin. 
        They were trained for three years by ShowBT Philippines, beginning in 
        2016.</Text>
        <Image style={styles.image2} source={require('./assets/sb19.png')} />
      <Button
        title="Go to Profile"
        onPress={() => navigation.navigate('Profile')}
      />
      <Button
        title="Go to Achievement"
        onPress={() => navigation.navigate('Achievement')}
      />
      <Button
        title="Go to Music List"
        onPress={() => navigation.navigate('Music List')}
      />
      
    </View>
  );
}
function ProfileScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', backgroundColor: '#55BCF6' }}>
      <Text style={styles.title}>"PABLO"</Text>
      <Text style={styles.info}>Name: John Paulo Nase</Text>
      <Text style={styles.info}>Nickname: Pablo</Text>
      <Text style={styles.info}>Birthdate: September 14, 1994</Text>
      <Text style={styles.info}>Birthplace: Imus Cavite</Text>
      <Text style={styles.info}>Position: Leader and Main Rapper</Text>
      <Image style={styles.image3} source={require('./assets/pablo.png')} />
      
      <Button
        title="Next"
        onPress={() => navigation.navigate('Stell')}/>
      </View>
  );
}
function NextScreen1({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center' , backgroundColor: '#55BCF6'}}>
      <Text style={styles.title}>"STELL"</Text>
      <Text style={styles.info}>Name: Stellvester Ajero</Text>
      <Text style={styles.info}>Nickname: Lester/Teytey</Text>
      <Text style={styles.info}>Birthdate: June 16, 1995</Text>
      <Text style={styles.info}>Birthplace: Las Piñas City </Text>
      <Text style={styles.info}>Position: Main Vocalist and Lead Dancer</Text>
      <Image style={styles.image4} source={require('./assets/stell.png')} />
      
      <Button
        title="Next"
        onPress={() => navigation.navigate('Ken')}
      />
    </View>
  );
}
function NextScreen2({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center' , backgroundColor:  '#55BCF6'}}>
      <Text style={styles.title}>"KEN"</Text>
      <Text style={styles.info}>Name: Felip John Suson</Text>
      <Text style={styles.info}>Nickname: Ken/Nek</Text>
      <Text style={styles.info}>Birthdate: January 12, 1997</Text>
      <Text style={styles.info}>Birthplace: Cagayan De Oro</Text>
      <Text style={styles.info}>Position: Main Dancer and Lead Vocalist</Text>
      <Image style={styles.image5} source={require('./assets/ken.png')} />
      <Button
        title="Next"
        onPress={() => navigation.navigate('Justin')}
      />
    </View>
  );
}
function NextScreen3({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', backgroundColor:  '#55BCF6'}}>
      <Text style={styles.title}>"JUSTIN"</Text>
      <Text style={styles.info}>Name: Justin de Dios</Text>
      <Text style={styles.info}>Nickname: Jah</Text>
      <Text style={styles.info}>Birthdate: July 7, 1998</Text>
      <Text style={styles.info}>Birthplace: Caloocan City </Text>
      <Text style={styles.info}>Position: Sub-Vocalist</Text>
      <Image style={styles.image6} source={require('./assets/justin.png')} />
      <Button
        title="Next"
        onPress={() => navigation.navigate('Josh')}
      />
    </View>
  );
}
function NextScreen4({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center' , backgroundColor:  '#55BCF6'}}>
      <Text style={styles.title}>"JOSH"</Text>
      <Text style={styles.info}>Name: Josh Cullen Santos</Text>
      <Text style={styles.info}>Nickname: Josh</Text>
      <Text style={styles.info}>Birthdate: October 22, 1993</Text>
      <Text style={styles.info}>Birthplace: Las Piñas City </Text>
      <Text style={styles.info}>Position: Main Rapper and Sub-Vocalist</Text>
      <Image style={styles.image7} source={require('./assets/josh.png')} />
      <Button 
        title="Go bact to details"
        onPress={() => navigation.navigate('Details')}
      />
    </View>
  );
}
function AchievementScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' , backgroundColor: '#55BCF6'}}>
      <Text style={{fontWeight: 'bold', fontSize: 20}}>
          * First Southeast Asian act to reach the top 10 of the magazine's annual chart. 
        On April 29, 2021, SB19 became the first Filipino and 
        Southeast Asian act to be nominated in Billboard Music Awards for Top Social Artist</Text>
        <Text style={{fontWeight: 'bold', fontSize: 20}}>
          * It marked the first-ever appearance of a Filipino artist in the Billboard Music Awards.</Text>
          <Text style={styles.text}>Wish Music Awards 2022</Text>
          <Text>Wish Group of the Year	SB19</Text>
          <Text>Wisher's Choice Award	SB19</Text>
          <Text>Wish Pop Song of the Year	"What"</Text>
          <Text>Wishclusive Pop Performance of the Year	"Ikako"</Text>
          <Text>Wishclusive Ballad Performance of the Year	"Hanggang Sa Huli"</Text>
          <Text style={styles.text}>Special Awards 2022</Text>
          <Text>Maharlikang Filipino Awards	2022</Text>
          <Text>Paragala: The Central Luzon Media Awards</Text>
          <Text style={styles.text}>International Awards 2022</Text>
          <Text>Dabeme Music Awards</Text>
          <Text>Prêmios Likes Brasil</Text>
          <Text>Prêmio Tudo Information</Text>
          <Text style={styles.text}>Others Awards 2022</Text>
          <Text>PPOP Awards Boy Group of the Year</Text>
          <Text>The New Hue Music Award Best Group Artist</Text>
          <Text style={styles.text}>Village Pipol Choice Award2022</Text>
          <Text>Group Performer of the Year</Text>
          <Text>Fandom of the Year"A'TIN"</Text>
          <Text>Composer of the Year "Pablo"</Text>
          <Text>Fashion Influencer of the Year "Ken"</Text>
      <Button 
        title="Back"
        onPress={() => navigation.navigate('Details')}
      />
    </View>
  );
}

function ListScreen({ navigation }) {
  return (
    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor:  '#55BCF6', }}>
      <Text style={styles.song}>Tilaluha</Text>
      <Text style={styles.song}>WMIAIN</Text>
      <Text style={styles.song}>Go Up</Text>
      <Text style={styles.song}>Love Goes</Text>
      <Text style={styles.song}>Alab</Text>
      <Text style={styles.song}>Ikako</Text>
      <Text style={styles.song}>Hanggang sa Huli</Text>
      <Text style={styles.song}>What</Text>
      <Text style={styles.song}>MAPA</Text>
      <Text style={styles.song}>Bazinga</Text>
      <Text style={styles.song}>Mana</Text>
      <Text style={styles.song}>SLMT</Text>
      <Button
        title="Back"
        onPress={() => navigation.navigate('Details')}
      />
    </View>
  );
}
const Stack = createNativeStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="A'TIN Guide App" component={HomeScreen} />
        <Stack.Screen name="Details" component={DetailsScreen} />
        <Stack.Screen name="Profile" component={ProfileScreen} />
        <Stack.Screen name="Stell" component={NextScreen1} />
        <Stack.Screen name="Ken" component={NextScreen2} />
        <Stack.Screen name="Justin" component={NextScreen3} />
        <Stack.Screen name="Josh" component={NextScreen4} />
        <Stack.Screen name="Achievement" component={AchievementScreen} />
        <Stack.Screen name="Music List" component={ListScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 24,
    backgroundColor: "#eaeaea"
  },
  title: {
    marginTop: 16,
    paddingVertical: 8,
    borderWidth: 4,
    borderColor: "#20232a",
    borderRadius: 6,
    backgroundColor: `#fff8dc`,
    color: "#20232a",
    textAlign: "center",
    fontSize: 30,
    fontWeight: "bold"
  },
  song: {
    marginTop: 16,
    paddingVertical: 0.2,
    borderWidth: 2,
    borderColor: "#20232a",
    borderRadius: 6,
    backgroundColor: `#fff8dc`,
    color: "#20232a",
    textAlign: "center",
    fontSize: 20,
    fontWeight: "bold" 
  },
  info: {
    fontSize: 20,
    backgroundColor: 'white',
    borderRadius: 5,
    marginRight: 15, 
    marginBottom: 15, 
    marginLeft:15,
  },
  text: {
    backgroundColor:`#fff8dc`,
     fontWeight: 'bold', 
     fontSize: 17,
     borderWidth: 2,
     borderColor: "#20232a",
     borderRadius: 6,
  },
  information:{ 
    fontSize: 20, 
    backgroundColor: 'white',
    borderRadius: 5,
    marginRight: 15, 
    marginBottom: 15, 
    marginLeft:15,
  },
  image1: {
    width: 300,
    height: 400,

  },
  image2: {
    width: 300,
    height: 200,
  },
  image3: {
    width: 300,
    height: 350,
  },
  image4: {
    width: 300,
    height: 350,
  },
  image5: {
    width: 300,
    height: 350,
  },
  image6: {
    width: 300,
    height: 350,
  },
  image7: {
    width: 300,
    height: 350,
  },
});

export default App;
