import { StatusBar } from 'expo-status-bar';
import { StyleSheet, Text, View, Alert } from 'react-native';
import { Button } from 'react-native-web';

function first(){
  alert(document.getElementById("output").value);
}
export default function App() {
  return (
    <View style={styles.container}>
    <input id='output' type="text" name='output'/>
    <button onClick={first}>Button</button>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
