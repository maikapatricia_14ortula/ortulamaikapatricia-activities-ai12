export default [
{
    id: '1',
    title: 'WHY IT?',
    description: '5 reasons why I choose IT',
    image: require('./assets/images/thinking.png'),

},
{
    id: '2',
    title: 'First Reason',
    description: 'Luckily, I was accepted into the university.',
    image: require('./assets/images/first.png'),
},
{
    id: '3',
    title: 'Second Reason',
    description:'It is my second option',
    image: require('./assets/images/second.png'),
},
{
    id: '4',
    title: 'Third Reason',
    description: 'I thought that there was no such thing as a math topic, and that IT was something along encoding text . Programming is not what I expect.',
    image: require('./assets/images/third.png'),
},
{
    id: '5',
    title: 'Fourth Reason',
    description: 'Jobs',
    image: require('./assets/images/fourth.png'),
},
{
    id: '6',
    title: 'Fifth Reason',
    description: 'I THOUGHT ITS EASY',
    image: require('./assets/images/fifth.png'),
}

];