import React, {useState, useRef } from 'react';
import {View, Text, StyleSheet, FlatList, Animated, Dimensions, TouchableOpacity } from "react-native";
import { StatusBar } from 'expo-status-bar';

import OnboardingItem from '../components/OnboardingItems';
import Paginator from '../components/Paginator';
import slides from '../slides';
import { Colors } from 'react-native/Libraries/NewAppScreen';
import OnboardingItems from '../components/OnboardingItems';

export default Onboarding =() => {
    const { width } = Dimensions.get('window');
    
    const [currentIndex, setCurrentIndex] = useState(0);
    const ref = useRef(null);

    const scrollX =useRef(new Animated.Value(0)).current;

    const viewableItemsChanged = useRef(({ viewableItems }) => {
        setCurrentIndex(viewableItems[0].index);
      }).current;
    
      const viewConfig = useRef({ viewAreaCoveragePercentThreshold: 50 }).current;
    
      
      const updateCurrentSlideIndex = (e) => {
        const contentOffsetX = e.nativeEvent.contentOffset.x;
        const currentIndex = Math.round(contentOffsetX / width);
        setCurrentIndex(currentIndex);
      };
     
      const goNextSlide = () => {
        const nextSlideIndex = currentIndex + 1;
        const offset = nextSlideIndex * width;
        ref?.current?.scrollToOffset({ offset });
      };
      const home = () => {
        const firstSlideIndex = slides.length - currentIndex;
        const offset = firstSlideIndex * currentIndex;
        ref?.current?.scrollToOffset({ offset });
        setCurrentIndex(firstSlideIndex);
      };
    return (
        <View style={styles.container}>
            <StatusBar backgroundColor='#e6e6fa' />
            <View style={{ flex: 3,backgroundColor: '#fff'}}>
            <FlatList 
            onMomentumScrollEnd={updateCurrentSlideIndex}
            data={slides} renderItem={({ item }) => <OnboardingItems item={item} />}
            horizontal 
            showsHorizontalScrollIndicator
            pagingenabled
            bounces={false}
            keyExtractor={(item => item.id)}
            onScroll={Animated.event([{nativeEvent:{contentOffset: {x: scrollX} } }], {
                useNativeDriver: false,
            })}
            scrollEventTrottle={32}
            onViewbleItemsChanged={viewableItemsChanged}
            viewabilityConfig={viewConfig}
            ref={ref}
            />

            </View>

            <Paginator data={slides} scrollX={scrollX}/>
            <View style={{ marginBottom: 50 }}>

        {currentIndex == slides.length - 1 ?
          (<View style={{ height: 50 }}>
            <TouchableOpacity style={[styles.button, { backgroundColor: 'maroon', borderWidth: 2 }]} onPress={home}>
              <Text style={{ fontWeight: 'bold', fontSize: 20, backgroundColor: '#e6e6fa', }}>
                Home</Text>
            </TouchableOpacity>
          </View>
          ) : (
            <View style={{ flexDirection: 'row' }}>
              <TouchableOpacity style={[styles.button, { backgroundColor: 'maroon', borderWidth: 2, }]} onPress={goNextSlide}>
                <Text style={{ fontWeight: 'bold', fontSize: 20, backgroundColor: '#e6e6fa' }}>
                  Continue</Text>
              </TouchableOpacity>
            </View>)
        }
            
        </View>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    }
})