import React from "react";
import {View, Text, StyleSheet, useWindowDimensions, Image } from "react-native";

export default OnboardingItems =({ item }) => {
    const { width } = useWindowDimensions();

    return (
        <View style={[styles.container, {width}]}>
          <Image source={item.image} style={[styles.image, { width, resizeMode: 'contain'}]}/>

          <View style= {{ flex: 0.3}}/>
            <Text style={styles.title}>{item.title}</Text>
            <Text style={styles.description}>{item.description}</Text>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    image: {
        flex: 3,
        justifyContent: 'center',
    },
    title: {
        fontWeight: '500',
        fontSize: 50,
        marginBottom: 10,
        color: 'maroon',
        textAlign: 'center',
        
    },
    description: {
        fontWeight: '200',
        fontSize: 20,
        color: 'black',
        textAlign: 'center',
        paddingHorizontal: 64,
    },
});
