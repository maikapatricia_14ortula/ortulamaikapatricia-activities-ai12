import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';

import TodoList from '../TodoList';
import CompleteTask from './screen/CompleteTask';


const todoName = 'To do list';
const homeName = 'Completed Task Go Here';

const Tab = createBottomTabNavigator();

export default function MainContainer() {
  return (
    <NavigationContainer>
      <Tab.Navigator
        initialRouteName={todoName}
        screenOptions={({ route }) => ({
          tabBarIcon: ({ focused, color, size }) => {
            let iconName;
            let rn = route.name;

            if (rn === todoName) {
              iconName = focused ? 'arrow-back-circle':'arrow-back-circle-outline'
            } else if (rn === homeName) {
              iconName = focused ? 'arrow-forward-circle':'arrow-forward-circle-outline'
            }

            return <Ionicons name={iconName} size={size} color={color} />

          },

        })}>

        
        <Tab.Screen name={todoName} component={TodoList} />
        <Tab.Screen name={homeName} component={CompleteTask} />


      </Tab.Navigator>
    </NavigationContainer>

  );
}



